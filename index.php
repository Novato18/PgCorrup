<?php 
include('fun/contador.php');
    ContadorV("1");
?>
<!DOCTYPE html>
<html>
<head>

	<title>No más mordidas</title>
    
    <?php include('head.html'); ?>
    <!-- UIkit JS -->
</head>


<body>
   <!-- <div class="loader"></div> -->


    <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
            <nav class="uk-navbar-container" uk-navbar="dropbar: false;" style="position: relative; z-index: 980;">
             <div class="uk-navbar-left">
                <div id="menuleft"></div>
             </div>
             <div class="uk-navbar-center">
                    <ul class="uk-navbar-nav">
                        <?php include('menu.html');?>
                    </ul>
             </div>
            <div class="uk-navbar-right">
                <div id="menuright"></div>

             </div>
            </nav>
        </div>
    
        <div id="encabezado"></div>

        <div style="max-height: 500px;">
             <div uk-parallax="bgy: -200" style="max-height: 500px;"> 
                <video autoplay loop muted playsinline style="width: 100%;height: 50%" >
                    <source src="../img/video/prueba1.mp4" type="video/mp4">
                    </video> 
                    <img src="../img/logo.png" style="max-height: 250px; max-width: 600px;" uk-cover>
            </div>
        </div>
	
    <div id="Cuerpo">
        <div class="uk-grid-match uk-grid-small uk-text-center uk-flex uk-flex-wrap uk-flex-wrap-around" uk-grid>
        
            <div class="uk-width-expand@m">
                <div class="uk-card uk-card-default uk-card-body">
                    <div style="text-align: right;">
                        
                        <h4> Visitas: <?php echo MostrarVisitas("1");?></h4>
                                    

                    </div>
                    
                    <div>
                        <div class="uk-height-large uk-background-cover uk-overflow-hidden uk-light uk-flex uk-flex-top" style="background-image: url('img/fondo1.jpg');">
                            <div class="uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
                                <h1 uk-parallax="opacity: 0,1; y: -100,0; scale: 2,1; viewport: 0.3;">¡Unete a nuestro Foro!</h1>
                                <p uk-parallax="opacity: 0,1; y: 100,0; scale: 0.5,1; viewport: 0.3;">Participa con otros ciudadanos a hablar sobre actos de corrupción y como solucionarlo <b>juntos</b></p>
                                <br>
                                <a href="foro" class="btn btn-primary" uk-parallax="opacity: 0,1; y: 100,50; scale: 0.5,2; viewport: 0.3;">Entra al foro</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div>
                        <div uk-grid>
                            <div class="uk-width-1-2">
                                <div class="uk-height-large uk-background-cover uk-overflow-hidden uk-light uk-flex uk-flex-top" style="background-image: url('img/fondo2.jpg');">
                            <div class="uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
                                <h3 uk-parallax="opacity: 0,1,1; y: -100,0,0; x: 100,100,0; scale: 2,1,1; viewport: 0.3;">Descubre que tan serca de ti ha ocurrido un acto de corrupción</h3>
                                <a href="informacion" class="btn btn-success" uk-parallax="opacity: 0,1,1; y: 100,0,0; x: 0,100,0; scale: 0.5,1,1; viewport: 0.3;">Entrar</a>
                            </div>
                        </div>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-height-large uk-background-cover uk-overflow-hidden uk-light uk-flex uk-flex-top" style="background-image: url('img/fondo3.jpg');">
                            <div class="uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
                                <h3 uk-parallax="opacity: 0,1,1; y: 100,0,0; x: -100,-100,0; scale: 0.5,1,1; viewport: 0.3;">¿Has precenciado alguna acción de corrupción y no sabes como denunciarlo?</h3>
                                <h4 uk-parallax="opacity: 0,1,1; y: 100,0,0; x: 0,100,0; scale: 0.5,1,1; viewport: 0.3;">¡Nosotros de ayudamos!</h4>
                                <a href="denuncia" class="btn btn-danger" uk-parallax="opacity: 0,1,1; y: 100,0,0; x: 0,100,0; scale: 0.5,1,1; viewport: 0.3;">¡Denuncia!</a>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div>
                        <div class="uk-height-large uk-background-cover uk-overflow-hidden uk-light uk-flex uk-flex-top" style="background-image: url('img/encabezado.jpg');" uk-parallax="target: #test-filter; blur: 0,02;">
                            <div class="uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
                                <h1 uk-parallax="opacity: 0,1; y: -100,0; scale: 2,1; viewport: 0.5; target: #test-filter; blur: 0;">No te hagas de la vista sorda</h1>
                                <h1 uk-parallax="opacity: 0,1; y: 100,0; scale: 0.5,1; viewport: 0.5; target: #test-filter; blur:0;">DENUNCIA!</h1>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="uk-width-1-4@m">
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body" >
                                
                                <div id="noticias">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body">
                                <h3>Videos</h3>
                                <hr>
                                <?php include('videos.html');  ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
    </div>
    <div id="pie" class="uk-background-secondary">
        <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
            <div class="uk-width-expand@m " id="pie">
        
    </div>
            </div>
        
    </div>

</body>
</html>