<?php 
include('fun/contador.php');
    ContadorV("6");
?>
<!DOCTYPE html>
<html>
<head>

    <title>No más mordidas</title>
    
    <?php include('head.html'); ?>
    <!-- UIkit JS -->
   
    
</head>


<body>
   <!-- <div class="loader"></div> -->


    <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
            <nav class="uk-navbar-container" uk-navbar="dropbar: true;" style="position: relative; z-index: 980;">
             <div class="uk-navbar-left">
                <div id="menuleft"></div>
             </div>
             <div class="uk-navbar-center">
                    <ul class="uk-navbar-nav">
                        <?php include('menu.html');  ?>
                    </ul>
             </div>
            <div class="uk-navbar-right">
                <div id="menuright"></div>

             </div>
            </nav>
        </div>
    
        <div id="encabezado"></div>
    </div>
        
    
    <div id="Cuerpo">
        <div class="uk-grid-match uk-grid-small uk-text-center uk-flex uk-flex-wrap uk-flex-wrap-around" uk-grid>
        
            <div class="uk-width-expand@m">
                <div class="uk-card uk-card-default uk-card-body">
                    <div style="text-align: right;">
                        
                        <h4> Visitas: <?php echo MostrarVisitas("6");?></h4>
                                    

                    </div>
                                  <table border="0" style="width: 100%" >
                <tr>
                    <!--
                    <td style="width: 30%">
                        <button class="uk-button uk-button-default"  style="width: 80%">Quienes somos?</button>
                    </td>
                    <th>Titulo</th> -->
                    <td rowspan="3" style="width: 20%">
                        <div class="uk-width-auto@m">
                            <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
                                <li><a href="#">ORIENTACIÓN LEGAL</a></li>
                                <li><a href="#">INICIA EL PROCESO</a></li>
                                <li><a href="#">SEGUIMIENTO DE DENUNCIA</a></li>
                            </ul>
                        </div>
                    </td>
                    <td rowspan="3">
                        <ul id="component-tab-left" class="uk-switcher">
                            <li>
                                <h2>ORIENTACIÓN LEGAL</h2>
                                <hr>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                
                            </li>
                            <li>
                                <h2>INICIA EL PROCESO</h2>
                                <hr>
                                <p>
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                                
                            </li>
                            <li>
                                <h2>SEGUIMIENTO DE DENUNCIA</h2>
                                <hr>
                                <p>
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur, sed do eiusmod.
                                </p>
                            
                        </ul>
                    </td>
                    
                </tr>

                </table>
                    

                </div>

            </div>

            <div class="uk-width-1-4@m">
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body" >
                                
                                <div id="noticias"></div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body">
                                <h3>Videos</h3>
                                <hr>
                                <?php include('videos.html');  ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
    </div>
    <div id="pie" class="uk-background-secondary">
        <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
            <div class="uk-width-expand@m " id="pie">
        
    </div>
            </div>
        
    </div>

</body>
</html>