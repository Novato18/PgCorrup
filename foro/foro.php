<!DOCTYPE html>
<html>
<?php 
include('../fun/contador.php');
    ContadorV("3");
?>
<head>

	<title>No más mordidas</title>
    
    <?php include('../head.html'); ?>
    <!-- UIkit JS -->
   
	
</head>


<body>
   <!-- <div class="loader"></div> -->


    <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
            <nav class="uk-navbar-container" uk-navbar="dropbar: true;" style="position: relative; z-index: 980;">
             <div class="uk-navbar-left">
                <div id="menuleft"></div>
             </div>
             <div class="uk-navbar-center">
                    <ul class="uk-navbar-nav">
                        <?php include('../menu.html');  ?>
                    </ul>
             </div>
            <div class="uk-navbar-right">
                <div id="menuright"></div>

             </div>
            </nav>
        </div>
    
        <div id="encabezado"></div>
    </div>
        
	
    <div id="Cuerpo">
        <div class="uk-grid-match uk-grid-small uk-text-center uk-flex uk-flex-wrap uk-flex-wrap-around" uk-grid>
        
            <div class="uk-width-expand@m">
                <div class="uk-card uk-card-default uk-card-body">
                    <div style="text-align: right;">
                        
                        <h4> Visitas: <?php echo MostrarVisitas("3");?></h4>
                                    

                    </div>
                    <?php
					require('configuracion.php');
					require('funciones.php');
					$id = $_GET["id"];
					if(empty($id)) Header("Location: index.php");

					$sql = "SELECT id, autor, titulo, mensaje, ";
					$sql.= "DATE_FORMAT(fecha, '%d/%m/%Y %H:%i:%s') as enviado FROM foro ";
					$sql.= "WHERE id='$id' OR identificador='$id' ORDER BY fecha ASC";
					$rs = mysqli_query($con,$sql );
					include('header.html');
					if(mysqli_num_rows($rs)>0)
					{
						//include('titulos_post.html');
						?>
						<table width="90%" border="0" align="center">
							<?php
						$template = implode("", file('post.html'));
						while($row = mysqli_fetch_assoc($rs))
						{
							//$color=($color==""?"#5b69a6":"");
							//$row["color"] = $color;
							//manipulamos el mensaje
							//$row["mensaje"] = nl2br($row["mensaje"]);
							//$row["mensaje"] = parsearTags($row["mensaje"]);
							//mostrarTemplate($template, $row);?>

							<tr>
								<td class="uk-background-muted"><h3><?php echo $row['titulo']; ?></h3> </td>
							</tr>
							<tr style="text-align: left;">
								<td>
									<article class="uk-comment uk-comment-primary">
									    <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
									        <div class="uk-width-auto">
									            <img class="uk-comment-avatar" src="../img/avatar.png" width="60" height="60" alt="">
									        </div>
									        <div class="uk-width-expand">
									            <h4 class="uk-comment-title uk-margin-remove"> <a class="uk-link-reset" href="#"><?php echo $row['autor'];?></a></h4>
									            <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
									                <li><?php echo $row['enviado'];?></li>
									                <li><a href="respuesta.php?id= <?php echo $row['id']; ?>&citar=1">CITAR</a> </li>
									            </ul>
									        </div>
									    </header>	
									    <div class="uk-comment-body">
									        <p><?php echo $row['mensaje']; ?></p>
									    </div>
									</article>
								</td>
							</tr>
						
							<?php
						}
					}
					?>
				</table>

                    

                </div>

            </div>

            <div class="uk-width-1-4@m">
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body" >
                                
                                <div id="noticias"></div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body">
                                <h3>Videos</h3>
                                <hr>
                                <?php include('../videos.html');  ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
    </div>
    <div id="pie" class="uk-background-secondary">
        <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
            <div class="uk-width-expand@m " id="pie">
        
    </div>
            </div>
        
    </div>

</body>
</html>