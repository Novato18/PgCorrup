

<!DOCTYPE html>
<html>
<?php 
include('../fun/contador.php');
    ContadorV("3");
?>
<head>

	<title>No más mordidas</title>
    
    <?php include('../head.html'); ?>
    <!-- UIkit JS -->
   
	
</head>


<body>
   <!-- <div class="loader"></div> -->


    <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
            <nav class="uk-navbar-container" uk-navbar="dropbar: true;" style="position: relative; z-index: 980;">
             <div class="uk-navbar-left">
                <div id="menuleft"></div>
             </div>
             <div class="uk-navbar-center">
                    <ul class="uk-navbar-nav">
                        <?php include('../menu.html');  ?>
                    </ul>
             </div>
            <div class="uk-navbar-right">
                <div id="menuright"></div>

             </div>
            </nav>
        </div>
    
        <div id="encabezado"></div>
    </div>
        
	
    <div id="Cuerpo">
        <div class="uk-grid-match uk-grid-small uk-text-center uk-flex uk-flex-wrap uk-flex-wrap-around" uk-grid>
        
            <div class="uk-width-expand@m">
                <div class="uk-card uk-card-default uk-card-body">
                    <div style="text-align: right;">
                        
                        <h4> Visitas: <?php echo MostrarVisitas("3");?></h4>
                                    

                    </div>
                    <?php
				require('configuracion.php');
				require('funciones.php');
				include('header.html');
				/* Pedimos todos los temas iniciales (identificador==0)
				* y los ordenamos por ult_respuesta */
				$sql = "SELECT id, autor, titulo, fecha, respuestas, ult_respuesta ";
				$sql.= "FROM foro WHERE identificador=0 ORDER BY ult_respuesta DESC";
				$rs = mysqli_query($con,$sql);
				if(mysqli_num_rows($rs)>0)
				{
					// Leemos el contenido de la plantilla de temas
					//$template = implode("", file("temas.html"));
					include('titulos.html'); 
					?>

					<table width="90%" border="0" align="center" cellpadding="2" cellspacing="2" class="uk-table-striped">

					<?php
					while($row = mysqli_fetch_assoc($rs))
					{
						//mostrarTemplate($template, $row);
						?>

							<tr> 
							    <td><a href="foro?id=<?php print $row['id'] ?>">
							      <?php print $row['titulo'] ?>
							      </a></td>
							    <td width="15%" align="center"><font size="-2">Por <b> 
							      <?php print $row['autor'] ?>
							      </b><br>
							      el 
							      <?php print $row['fecha'] ?>
							      </font></td>
							    <td width="15%" align="center"><font size="-2"> 
							      <?php print $row['respuestas'] ?>
							      </font></td>
							    <td width="15%" align="center"><font size="-2"> 
							      <?php print $row['ult_respuesta'] ?>
							      </font></td>
							  </tr>

						<?php
					}
					}

					?>
					</table>
                    

                </div>

            </div>

            <div class="uk-width-1-4@m">
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body" >
                                
                                <div id="noticias"></div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body">
                                <h3>Videos</h3>
                                <hr>
                                <?php include('../videos.html');  ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
    </div>
    <div id="pie" class="uk-background-secondary">
        <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
            <div class="uk-width-expand@m " id="pie">
        
    </div>
            </div>
        
    </div>

</body>
</html>