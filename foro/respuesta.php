<!DOCTYPE html>
<html>
<?php 
include('../fun/contador.php');
    ContadorV("3");
?>
<head>

	<title>No más mordidas</title>
    
    <?php include('../head.html'); ?>
    <!-- UIkit JS -->
   
	
</head>


<body>
   <!-- <div class="loader"></div> -->


    <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
            <nav class="uk-navbar-container" uk-navbar="dropbar: true;" style="position: relative; z-index: 980;">
             <div class="uk-navbar-left">
                <div id="menuleft"></div>
             </div>
             <div class="uk-navbar-center">
                    <ul class="uk-navbar-nav">
                        <?php include('../menu.html');  ?>
                    </ul>
             </div>
            <div class="uk-navbar-right">
                <div id="menuright"></div>

             </div>
            </nav>
        </div>
    
        <div id="encabezado"></div>
    </div>
        
	
    <div id="Cuerpo">
        <div class="uk-grid-match uk-grid-small uk-text-center uk-flex uk-flex-wrap uk-flex-wrap-around" uk-grid>
        
            <div class="uk-width-expand@m">
                <div class="uk-card uk-card-default uk-card-body">
                    <div style="text-align: right;">
                        
                        <h4> Visitas: <?php echo MostrarVisitas("3");?></h4>
                                    

                    </div>
                  <?php
					require('funciones.php');
					$id = $_GET["id"];
					$citar = $_GET["citar"];
					$row = array("id" => $id);
					if($citar==1)
					{
						require('configuracion.php');
						$sql = "SELECT titulo, mensaje, identificador AS id FROM foro WHERE id='$id'";
						$rs = mysqli_query($con,$sql );
						if(mysqli_num_rows($rs)==1) $row = mysqli_fetch_assoc($rs);
						$row["titulo"] = "Re: ".$row["titulo"];
						$row["mensaje"] = "[citar]".$row["mensaje"]."[/citar]";
						if($row["id"]==0) $row["id"]=$id;
					}
					$template = implode("", file('formulario.html'));
					include('header.html');
					mostrarTemplate($template, $row);
					include('footer.html');
				?>
                    

                </div>

            </div>

            <div class="uk-width-1-4@m">
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body" >
                                
                                <div id="noticias"></div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body">
                                <h3>Videos</h3>
                                <hr>
                                <?php include('../videos.html');  ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
    </div>
    <div id="pie" class="uk-background-secondary">
        <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
            <div class="uk-width-expand@m " id="pie">
        
    </div>
            </div>
        
    </div>

</body>
</html>