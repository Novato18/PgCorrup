<?php 
include('fun/contador.php');
    ContadorV("7");
?>
<!DOCTYPE html>
<html>
<head>

    <title>No más mordidas</title>
    
    <?php include('head.html'); ?>
    <!-- UIkit JS -->
   
    
</head>


<body>
   <!-- <div class="loader"></div> -->


    <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
            <nav class="uk-navbar-container" uk-navbar="dropbar: true;" style="position: relative; z-index: 980;">
             <div class="uk-navbar-left">
                <div id="menuleft"></div>
             </div>
             <div class="uk-navbar-center">
                    <ul class="uk-navbar-nav">
                        <?php include('menu.html');  ?>
                    </ul>
             </div>
            <div class="uk-navbar-right">
                <div id="menuright"></div>

             </div>
            </nav>
        </div>
    
        <div id="encabezado"></div>
    </div>
        
    
    <div id="Cuerpo">
        <div class="uk-grid-match uk-grid-small uk-text-center uk-flex uk-flex-wrap uk-flex-wrap-around" uk-grid>
        
            <div class="uk-width-expand@m">
                <div class="uk-card uk-card-default uk-card-body">
                    <div style="text-align: right;">
                        
                        <h4> Visitas: <?php echo MostrarVisitas("7");?></h4>
                                    

                    </div>
                    <h3 class="uk-card-title">Si tienes alguna duda, contáctanos</h3>
            <p>¡Gracias!</p>
            <div class="uk-card uk-card-default uk-card-body uk-width-3-4 uk-align-center">
                <div class="uk-margin">
                    <textarea class="uk-textarea"  rows="5" placeholder="Dejar comentario"></textarea>
                </div>

                <div class="uk-margin">
                    <input class="uk-input" type="text" placeholder="Nombre (opcional)">
                </div>

                <div class="uk-margin">
                    <input class="uk-input" type="text" placeholder="Apellido (opcional)">
                </div>

                <div class="uk-margin">
                    <input class="uk-input" type="number" placeholder="celular (opcional)">
                </div>

                <div class="uk-margin">
                    <input class="uk-input" type="text" placeholder="Correo electronico">
                </div>

                <button class="uk-button uk-button-default">Enviar comentarios</button>


            </div>
                    
                    

                </div>

            </div>

            <div class="uk-width-1-4@m">
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body" >
                                
                                <div id="noticias"></div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
                        <div class="uk-width-expand">
                            <div class="uk-card uk-card-default uk-card-body">
                                <h3>Videos</h3>
                                <hr>
                                <?php include('videos.html');  ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
    </div>
    <div id="pie" class="uk-background-secondary">
        <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
            <div class="uk-width-expand@m " id="pie">
        
    </div>
            </div>
        
    </div>

</body>
</html>